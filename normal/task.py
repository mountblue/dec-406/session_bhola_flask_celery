import time
from celery import Celery

app = Celery('task', backend="amqp", broker='amqp://guest@localhost//')

@app.task
def add(x, y):
    return x + y

@app.task
def gen_table():
    all_table = []
    for i in range(100):
        table = {}
        for j in range(1,11):
            table[j] = i*j
        time.sleep(0.5)
        all_table.append(table)
    
    return all_table
