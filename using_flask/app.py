import os
import time
from flask import Flask, render_template, url_for, jsonify
from celery import Celery


app = Flask(__name__)
app.config['SECRET_KEY'] = 'asdfghjklq1234566789@&*&$*BHR%^VGHVH$GGHFY'


# Celery configuration
app.config['CELERY_BROKER_URL'] = 'amqp://guest@localhost//'
app.config['CELERY_RESULT_BACKEND'] = 'amqp://guest@localhost//'

# Initialize Celery
celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)

APP_ROOT = os.path.dirname(os.path.abspath(__file__))


@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')


@app.route('/longtask', methods=['POST'])
def longtask():
    task = long_task.apply_async()
    return jsonify({}), 202, {'Location': url_for('taskstatus',
                                                  task_id=task.id)}


@celery.task(bind=True)
def long_task(self):
    all_table = []
    for i in range(1, 101):
        table = {}
        for j in range(1,11):
            table[j] = i*j
        time.sleep(0.5)
        self.update_state(state='PROGRESS',
                          meta={'current': i, 'status': 'In progress...'})
        all_table.append(table)

    filename = APP_ROOT  + '/static/data.txt'
    
    with open(filename, 'w') as f:
        for item in all_table:
            f.write("%s\n" % item)
    
    return {
            'current': 100,
            'status': 'Task Completed',
            'filename': 'data.txt'
        }


@app.route('/status/<task_id>')
def taskstatus(task_id):
    task = long_task.AsyncResult(task_id)
    if task.state == 'PENDING':
        response = {
            'state': task.state,
            'current': 0,
            'status': 'Pending...'
        }
    elif task.state != 'FAILURE':
        response = {
            'state': task.state,
            'current': task.info.get('current', 0),
            'status': task.info.get('status', '')
        }
        if 'filename' in task.info:
            response['filename'] = task.info['filename']
    else:
        # something went wrong in the background job
        response = {
            'state': task.state,
            'current': 1,
            'status': str(task.info),  # this is the exception raised
        }
    return jsonify(response)


if __name__ == '__main__':
    app.run(debug=True)
